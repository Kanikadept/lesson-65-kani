const axios = require("axios");

const axiosFireBase = axios.create({
    baseURL: 'https://lesson-65-kani-default-rtdb.firebaseio.com/'
});

export default axiosFireBase;
