import './App.css';
import {Switch, Route} from 'react-router-dom'
import Layout from "./components/Layout/Layout";
import PageContent from "./containers/PageContent/PageContent";
import EditPage from "./containers/EditPage/EditPage/EditPage";

const App = () => (
    <div className="App">
      <div className="container">
          <Layout>
              <Switch>
                  <Route path="/" exact component={PageContent}/>
                  <Route path="/pages/:id" component={PageContent}/>
                  <Route path="/admin" component={EditPage}/>
              </Switch>
          </Layout>
      </div>
    </div>
);

export default App;
