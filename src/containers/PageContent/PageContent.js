import React, {useEffect, useState} from 'react';
import axiosFireBase from "../../axios-FireBase";
import './PageContent.css';

const PageContent = (props) => {

    const [page, setPage] = useState({});



    useEffect(() => {
        const fetchData = async () => {
            if (props.match.params.id) {
                const pageResponse = await axiosFireBase.get(`/pages/${props.match.params.id}.json`);
                setPage(pageResponse.data);
            } else {
                const pageResponse = await axiosFireBase.get(`/pages/home.json`);
                setPage(pageResponse.data);
            }
        }

        fetchData().catch(console.error);
    }, [props.match.params.id])

    return (
        <div className="page-content">
            <strong>Page title:</strong>
            <p> {page && page.title}</p>
            <i>Page content: </i>
            <p>{page && page.content}</p>

        </div>
    );
};

export default PageContent;