import React from 'react';
import './EditPage.css';
import axiosFireBase from "../../../axios-FireBase";
import {useEffect, useState} from "react";

const EditPage = (props) => {
    const [pages, setPages] = useState([]);
    const [selectedPage, setSelectedPage] = useState('');
    const [pageContent, setPageContent] = useState({
        'title': '',
        'content': '',
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosFireBase.get('/pages.json');
            const pageArr = Object.keys(response.data);
            setPages(pageArr);
            setSelectedPage(pageArr[0]);
        }
        fetchData().catch(console.error);
    }, []);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosFireBase.get(`/pages/${selectedPage}.json`);

            setPageContent(pageContent => ({...pageContent,
                title: response.data.title, content: response.data.content}));
        }
        fetchData().catch(console.error);
    }, [selectedPage]);

    const handleChangePage = (event) => {
        setSelectedPage(event.target.value);
    }

    const handleSelectPage = (event) => {
        const {name, value} = event.target;
        setPageContent(pageContent => ({...pageContent, [name]: value}));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        await axiosFireBase.put(`/pages/${selectedPage}.json`, pageContent);
        props.history.push(`/pages/${selectedPage}`);

    }

    return (
        <form className="edit-page" onSubmit={handleSubmit}>
            <p>Edit pages</p>
            <label>select page</label>
            <div className="form-row">
                <select onChange={handleChangePage}>
                    {pages.map(page => {
                        return <option key={page} value={page}>{page.toUpperCase()}</option>;
                    })}
                </select>
            </div>
            <label>Title</label>
            <div className="form-row">
                <input type="text" name="title" onChange={handleSelectPage} value={pageContent.title}/>
            </div>

            <label>Content</label>
            <div className="form-row">
                <textarea name="content" onChange={handleSelectPage} value={pageContent.content}/>
            </div>
            <button>Save</button>
        </form>
    );
};

export default EditPage;