import React from 'react';
import './ToolBar.css';
import NavigationItems from "../NavigationItems/NavigationItems";


const ToolBar = () => {
    return (
        <header className="tool-bar">
            <div>Static Pages</div>
            <nav>
                <NavigationItems/>
            </nav>
        </header>
    );
};

export default ToolBar;