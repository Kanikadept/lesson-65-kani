import React from 'react';
import ToolBar from "../Navigation/ToolBar/ToolBar";
import './Layout.css';

const Layout = props => {
    return (
       <>
           <ToolBar />
           <main className="Layout-Content">
               {props.children}
           </main>
       </>
    );
};

export default Layout;